<html>
<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
</head>

</html>

<?php
require_once("../../../vendor/autoload.php");

use App\BookTitle\BookTitle;
use App\Message\Message;



$objBookTitle= new BookTitle();

$objBookTitle->index("obj");    // obj for object return

$allData=$objBookTitle->index("obj");

$serial=1;

echo "<table border='2px'>";
echo "<th>Serial</th><th>ID</th><th>Book Title</th><th>Author Name</th><th>Action</th>";


foreach($allData as $oneData){

    echo "<tr style='height: 40px'>";

    echo "<td> $serial</td>>";
    echo "<td>$oneData->id</td>>";
    echo "<td>$oneData->book_title</td>>";
    echo "<td>$oneData->author_name</td>>";

    echo "
            <td>
             <a href='view.php?id=$oneData->id'><button class='btn-info'>View</button></a>
             <a href='edit.php?id=$oneData->id'><button class='btn-primary'>Edit</button></a>
             <a href='delete.php?id=$oneData->id'><button class='btn-danger'>Delete</button></a>

           </td>
    ";

    echo "</tr>";
    $serial++;

}
echo "</table>";