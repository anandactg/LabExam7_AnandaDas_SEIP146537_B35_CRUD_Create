<?php
namespace App\Gender;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Gender extends DB{
    public $id="";
    public $name="";
    public $gender="";
    public function __construct(){



        parent::__construct();

    }
    public function index(){

        echo "";
        echo "<br>";
        echo $this->gender;
    }


    public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){

            $this->id=$postVariabledata['id'];
        }

        if(array_key_exists('name',$postVariabledata)){
            $this->name=$postVariabledata['name'];
        }

        if(array_key_exists('gender',$postVariabledata)){
            $this->gender=$postVariabledata['gender'];
        }
    }

    public function store(){

        $arrData=array($this->name, $this->gender);
        $sql="INSERT INTO gender(name, gender) VALUES (?,?)";
        // $sql="insert INTO book_title(book_title, author_name) VALUES ('$this->book_title', '$this->author_name')"; // for stop SQL Injection Hacking we need to use value(?,?)

        $STH=$this->DBH->prepare($sql);    // $sql returning object in $STH. prepare method returns object.

        //$STH->execute();
        $result=$STH->execute($arrData);

        if($result)
            Message::Message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::Message("Failed! Data Has not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }// end of store method

}