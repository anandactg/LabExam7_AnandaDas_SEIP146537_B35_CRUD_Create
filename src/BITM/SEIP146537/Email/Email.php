<?php
namespace App\Email;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Email extends DB{
    public $id="";
    public $name="";
    public $email="";

    public function __construct(){



        parent::__construct();

    }
    public function index(){
        echo "";
        echo "<br>";
        echo $this->email;
    }


   public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){

            $this->id=$postVariabledata['id'];
        }

        if(array_key_exists('name',$postVariabledata)){
            $this->name=$postVariabledata['name'];
        }

        if(array_key_exists('email',$postVariabledata)){
            $this->email=$postVariabledata['email'];
        }
    }

    public function store(){

        $arrData=array($this->name, $this->email);
        $sql="INSERT INTO email(name, email) VALUES (?,?)";
        // $sql="insert INTO book_title(book_title, author_name) VALUES ('$this->book_title', '$this->author_name')"; // for stop SQL Injection Hacking we need to use value(?,?)

        $STH=$this->DBH->prepare($sql);    // $sql returning object in $STH. prepare method returns object.

        //$STH->execute();
        $result=$STH->execute($arrData);

        if($result)
            Message::Message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::Message("Failed! Data Has not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }// end of store method

}