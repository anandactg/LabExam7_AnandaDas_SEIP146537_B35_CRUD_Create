-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2016 at 08:20 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_b35`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `birth_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birth_date`) VALUES
(1, 'Ananda Das', '2016-11-16'),
(3, 'Bill Gates', '2016-11-14'),
(4, 'Marc Jukerbag', '2016-11-20'),
(5, 'Ali bhai', '0000-00-00'),
(6, 'dlkadklsaj', '2016-11-09'),
(7, 'Mohammad ALi', '2016-11-23'),
(8, 'Robert Patinson', '2016-11-09');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_title` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`) VALUES
(1, 'Introduction to Programming', 'Tamim Saharier Subin'),
(2, 'Programming for Dummies', 'Alester fonn'),
(5, 'Ananda', 'ALi'),
(6, 'Introduction to PHP', 'Ananda'),
(7, 'Programming C', 'BITM'),
(8, 'Hello5', 'hello'),
(9, 'Hello5', 'hello'),
(10, 'New book', 'new'),
(11, 'Notun Book', 'bill'),
(12, 'Helloworld', 'Ananda'),
(13, 'What a memory', 'memory'),
(14, 'Programming C', 'martin C luther'),
(15, 'Introduction to HTML', 'Md. ALi');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `state`, `country`) VALUES
(1, 'Ananda Das', 'Chittagong', 'Bangladesh'),
(2, 'Bill Gates', 'New York', 'USA'),
(3, 'sds', 'Delvine', 'Albania'),
(4, 'tusar', 'Dhaka', 'Bangladesh'),
(5, 'Anadna', 'Delvine', 'Albania'),
(6, 'ananda', 'Bulqize', 'Albania'),
(7, 'Charles', 'Andorra la Vella', 'Angola'),
(8, 'Ananda', 'Chittagong', 'Bangladesh');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`) VALUES
(3, 'Ananda Das', 'anandactgbd@gmail.com'),
(4, 'Bill Gates', 'gatesfoundation@hotmail.com'),
(5, 'hello', 'hekek@gmal.com'),
(6, 'Jason homer', 'anandsa@email.com');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `gender` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`) VALUES
(1, 'Ananda Das', 'male'),
(2, 'Aishwariya Rai', 'Female'),
(3, 'Ananda', 'male'),
(4, 'jdkakj', 'jdklajlkkdaj'),
(5, 'Ananda', 'male'),
(6, 'Robert Patinson', 'male');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `hobbies` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`) VALUES
(1, 'Ananda Das', 'Programming, Translating, Reading'),
(2, 'Bill Gates', 'Programming, Making Money'),
(3, 'Ananda', 'working'),
(4, 'kjkajdklaj', 'kdjakjdkal,jkdakjla'),
(10, 'Ananda', 'Gardening,Gaming,HTML/CSS'),
(11, 'rahul', 'Gardening,HTML/CSS'),
(12, 'karima', 'Gaming,HTML/CSS,HTML/CSS'),
(13, 'Ananda', 'Gardening,Gaming,HTML/CSS,HTML/CSS'),
(14, 'Md Ali', 'Gaming,Reading,HTML/CSS'),
(15, 'Mohammad ALi', 'Gardening,Gaming,Reading');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `image`) VALUES
(1, 'Ananda Das', 'http://anandayogahome.com/wp-content/uploads/2016/01/anandalogo-1.png'),
(2, 'Bill Gates', 'http://www.lifehack.org/329312/5-book-recommendations-from-bill-gates'),
(3, 'ananda', 'pic here'),
(4, 'Ananda', '1478932363pexels-photo-27714-medium.jpg'),
(5, 'Ali bhai', '1478932476Capture.PNG'),
(6, 'Ananda', '1478934346flower.jpg'),
(7, 'Ananda', '1478934392flower.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE IF NOT EXISTS `summary_of_organization` (
  `id` int(11) NOT NULL,
  `organization_name` varchar(200) NOT NULL,
  `organization_summary` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `organization_name`, `organization_summary`) VALUES
(0, 'BITM', 'Basis training Center'),
(0, 'Microsoft', 'Software Company'),
(0, 'asa', 'asaasasasasa'),
(0, 'asa', 'asaasasasasa'),
(0, 'sss', 'wwww'),
(0, 'Ananda', ''),
(0, ' BASIS', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
